const subMessage = {
    userAdded: 'USERADDED',
    userLogin: 'USERLOGIN',
    messageAdded: 'MESSAGEADDED'
}

module.exports = subMessage;