const { UserTC } = require("../model/user");
const { UserSubscription } = require("./user.mutation");
const subMessage = require('../subMsgs')
const authMiddleware = require("../middleware/auth-middleware");

const UserQuery = {
  userById: UserTC.mongooseResolvers.findById().withMiddlewares([authMiddleware]),
  userMany: UserTC.mongooseResolvers.findMany().withMiddlewares([authMiddleware]),
  userCount: UserTC.mongooseResolvers.count().withMiddlewares([authMiddleware]),
  userConnection: UserTC.mongooseResolvers.connection().withMiddlewares([authMiddleware]),
  userPagination: UserTC.mongooseResolvers.pagination().withMiddlewares([authMiddleware]),
};

const UserMutation = {
  userCreateOne: UserTC.mongooseResolvers.createOne({ record: { removeFields: ['_id', 'status', 'last_active', 'created_at', 'updated_at'] } }),
  userRemoveById: UserTC.mongooseResolvers.removeById().withMiddlewares([authMiddleware]),
  // fakeData: UserTC.getResolver("user", [authMiddleware]),
  userLogin: UserTC.getResolver("login")
};


module.exports = { UserQuery, UserMutation, UserSubscription };
