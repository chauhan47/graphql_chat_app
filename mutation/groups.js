const authMiddleware = require("../middleware/auth-middleware");
const { GroupSchema, GroupTC } = require("../model/group");


const GroupQuery = {
    groupById: GroupTC.mongooseResolvers.findById().withMiddlewares([authMiddleware]),
    groupByIds: GroupTC.mongooseResolvers.findByIds().withMiddlewares([authMiddleware]),
    groupOne: GroupTC.mongooseResolvers.findOne().withMiddlewares([authMiddleware]),
    groupMany: GroupTC.mongooseResolvers.findMany().withMiddlewares([authMiddleware]),
    groupCount: GroupTC.mongooseResolvers.count().withMiddlewares([authMiddleware]),
    groupConnection: GroupTC.mongooseResolvers.connection().withMiddlewares([authMiddleware]),
    groupPagination: GroupTC.mongooseResolvers.pagination().withMiddlewares([authMiddleware]),
};

const GroupMutation = {
    groupCreateOne: GroupTC.mongooseResolvers.createOne({record: {removeFields: ['_id', 'users', 'created_at', 'updated_at']}}).withMiddlewares([authMiddleware]),
    groupRemoveById: GroupTC.mongooseResolvers.removeById().withMiddlewares([authMiddleware]),
};

module.exports = { GroupQuery, GroupMutation };
