const { SchemaComposer } = require('graphql-compose');

const schemaComposer = new SchemaComposer();

const { UserQuery, UserMutation, UserSubscription } = require('./user');
const { MessageQuery, MessageMutation } = require('./message');
const { GroupQuery, GroupMutation } = require('./groups');

schemaComposer.Query.addFields({
    ...UserQuery,
    ...MessageQuery,
    ...GroupQuery
});

schemaComposer.Subscription.addFields({
    ...UserSubscription,
})

schemaComposer.Mutation.addFields({
    ...UserMutation,
    ...MessageMutation,
    ...GroupMutation
});

module.exports = schemaComposer.buildSchema();