const faker = require("faker");
const { UserTC, UserSchema } = require("../model/user");
const { schemaComposer, toInputObjectType } = require('graphql-compose');
const jwt = require('jsonwebtoken');
const config = require("../config");
const subMessage = require("../subMsgs");
const resolver = function () { };

const MyCustomAuth = schemaComposer.createObjectTC({
  name: 'Login',
  fields: {
    userId: "MongoID",
    email: 'String',
    token: 'String'
  }
});
const MyMessageType = schemaComposer.createObjectTC({
  name: 'messageType',
  fields: {
    senderId: "MongoID!",
    messageId: "MongoID!",
    senderName: "String!",
    body: 'String!'
  }
});


resolver.userLogin = UserTC.addResolver({
  kind: 'mutation',
  name: 'login',
  type: MyCustomAuth,
  args: { email: 'String!', password: 'String!' },
  resolve: async ({ args }) => {
    return new Promise(async (resolve, reject) => {
      const { email, password } = args;
      const user = await UserSchema.findOne({ email });
      if (!user) reject('Authentication Failed');
      const token = jwt.sign(user.toJSON(), config.user_jwt.secret_key, {
        expiresIn: config.user_jwt.expires_in
      });
      resolve({
        userId: user._id,
        email: user.email,
        token: token
      });
    });
  }
});

const UserSubscription = {
  messageAdded: {
    type: MyMessageType,
    args: { channelId: "ID!" },
    description: "Subscribe to messageAdded",
    resolve: (payload) => {
      console.log(payload)
      return payload
    },
    subscribe: (_, __, { pubsub }) => pubsub.asyncIterator(subMessage.messageAdded)
  }
}

module.exports = { resolver, UserSubscription };
