const faker = require("faker");
const { MessageTC, MessageSchema } = require("../model/message");
const { schemaComposer, toInputObjectType } = require('graphql-compose');
const jwt = require('jsonwebtoken');
const config = require("../config");
const subMessage = require("../subMsgs");
const { UserSchema } = require("../model/user");
const resolver = function () { };

resolver.createMessage = MessageTC.addResolver({
    name: "createMessage",
    type: MessageTC,
    args: { sender: 'MongoID!', body: 'String!', groupId: 'MongoID!' },
    resolve: async ({ source, args, context, info }) => {
        const userInfo = await UserSchema.findById(args.sender);
        let message = new MessageSchema({
            senderId: args.sender,
            senderName: userInfo.name,
            body: args.body,
            groupId: args.groupId,
        });
        const messageRes = await message.save();
        context.pubsub.publish(subMessage.messageAdded, { senderId: args.sender, senderName: userInfo.name, messageId: messageRes._id, body: messageRes.body });
        return messageRes;
    },
});

module.exports = resolver;