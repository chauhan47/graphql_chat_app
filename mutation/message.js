const authMiddleware = require("../middleware/auth-middleware");
require("./message.mutation");
const { MessageSchema, MessageTC } = require("../model/message");

const MessageQuery = {
  messageById: MessageTC.mongooseResolvers.findById().withMiddlewares([authMiddleware]),
  messageByIds: MessageTC.mongooseResolvers.findByIds().withMiddlewares([authMiddleware]),
  messageCount: MessageTC.mongooseResolvers.count().withMiddlewares([authMiddleware]),
  messageConnection: MessageTC.mongooseResolvers.connection().withMiddlewares([authMiddleware]),
  messagePagination: MessageTC.mongooseResolvers.pagination().withMiddlewares([authMiddleware]),
};

const MessageMutation = {
  createMessage: MessageTC.getResolver('createMessage', [authMiddleware]),
  messageRemoveById: MessageTC.mongooseResolvers.removeById().withMiddlewares([authMiddleware]),
};

module.exports = { MessageQuery: MessageQuery, MessageMutation: MessageMutation };
