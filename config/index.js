const dotenv = require('dotenv')
dotenv.config()

const config = {
  port: process.env.APP_PORT,
  db_name: process.env.DB_NAME,
  db_user: process.env.DB_USER,
  db_password: process.env.DB_PASSWORD,
  db_host: process.env.DB_HOST,
  db_dialect: process.env.DB_DIALECT,
  db_port: process.env.DB_PORT,
  admin_jwt: {
    secret_key: process.env.ADMIN_JWT_SECRET,
    expires_in: process.env.ADMIN_JWT_EXPIRESIN,
  },
  user_jwt: {
    secret_key: process.env.USER_JWT_SECRET,
    expires_in: process.env.USER_JWT_EXPIRESIN,
  },
  aws: {
    accessKeyId: process.env.AWSID,
    secretAccessKey: process.env.AWSSECRETID,
    bucketName: process.env.BUCKET_NAME
  },
  crypto: {
    passSecretKey: process.env.USER_CRPTO_SECRET
  },
  pwd: {
    reset_expires_in: process.env.PWD_RESET_JWT_EXPIRESIN
  }
}

module.exports = config
