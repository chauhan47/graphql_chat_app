const jwt = require('jsonwebtoken');
const config = require('../config');

const authMiddleware = (resolve, source, args, context, info) => {
    const req = context.req;
    const authHeader = req.get('Authorization');
    if (!authHeader) {
        req.error = "No authentication header found."
        req.isAuth = false
        return context.res.json({ data: {}, success: false, err: req.error, status_code: 401 })
    }

    let decoded

    try {
        const token = authHeader.split(' ')[1]
        decoded = jwt.verify(token, config.user_jwt.secret_key)
    } catch (error) {
        req.isAuth = false
        req.error = error.message
        return context.res.json({ data: {}, success: false, err: req.error, status_code: 401 })
    }

    if (!decoded) {
        req.isAuth = false
        req.error = "Unable to decode jwt"
        return context.res.json({ data: {}, success: false, err: req.error, status_code: 401 })

    }

    req.isAuth = true
    req.userId = decoded._id
    req.userName = decoded.name
    req.error = null
    return resolve(source, args, context, info)
}

module.exports = authMiddleware