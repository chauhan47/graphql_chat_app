const { ApolloServer } = require("apollo-server");
require('./database')
const { execute, subscribe } = require('graphql');
const pubsub = require('./helpers/pubsub')
const { SubscriptionServer } = require('subscriptions-transport-ws');
const graphqlSchema = require("./mutation/index");

const server = new ApolloServer({
  schema: graphqlSchema,
  context: ({ req, res }) => ({ req, res, pubsub })
});

server.listen(5000).then(({ url }) => {
  console.log(`server started at ${url}`)
  // new SubscriptionServer(
  //   {
  //     schema:graphqlSchema,
  //     execute,
  //     subscribe,
  //     onConnect: () => console.log('Client connected')
  //   },
  //   {
  //     server,
  //     path: '/subscriptions'
  //   }
  // )
});