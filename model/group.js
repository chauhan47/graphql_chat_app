const mongoose = require("mongoose");
const { composeMongoose } = require("graphql-compose-mongoose");
const Schema = mongoose.Schema;

var GroupSchema = new Schema({
    name: { type: String, required: true },
    users: [{
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'UserSchema' }
    }],
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'UserSchema', required: true },
}, {
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" }
});

module.exports = {
    GroupSchema: mongoose.model("groups", GroupSchema),
    GroupTC: composeMongoose(mongoose.model("groups", GroupSchema)),
};
