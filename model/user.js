const mongoose = require("mongoose");
const { composeMongoose } = require("graphql-compose-mongoose");
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const User = new Schema(
  {
    // _id: Schema.Types.ObjectId,
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    phone: { type: String, required: true, unique: true },
    password: { type: String },
    status: { type: Boolean },
    last_active: { type: Date, default: Date.now },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

User.pre('save', async function save(next) {
  if (!this.isModified('password')) return next();
  try {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
    return next();
  } catch (err) {
    return next(err);
  }
});

User.methods.validatePassword = async function validatePassword(data) {
  return bcrypt.compare(data, this.password);
};

module.exports = {
  UserSchema: mongoose.model("users", User),
  UserTC: composeMongoose(mongoose.model("users", User)),
};
