const mongoose = require("mongoose");
const { composeMongoose } = require("graphql-compose-mongoose");
const Schema = mongoose.Schema;

var MessageSchema = new Schema({
    senderId: { type: Schema.Types.ObjectId, ref: 'User' },
    senderName: { type: "String" },
    body: String,
    meta: [{
        user: { type: Schema.Types.ObjectId, ref: 'User' },
        delivered: Boolean,
        read: Boolean
    }],
    groupId: { type: Schema.Types.ObjectId, ref: "groups" },
}, {
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" }
});

module.exports = {
    MessageSchema: mongoose.model("messages", MessageSchema),
    MessageTC: composeMongoose(mongoose.model("messages", MessageSchema)),
};
